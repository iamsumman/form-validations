
<?php
/*
Author: Javed Ur Rehman
Website: https://www.allphptricks.com/
*/
 
require('db.php');
include("auth.php");

$status = "";
function getAge($then) {
    $then = date('Ymd', strtotime($then));
    $diff = date('Ymd') - $then;
    return substr($diff, 0, -4);
}
@$gender=$_POST['sex'];
@$opp= $_POST['op'];
if(isset($_POST['new']) && $_POST['new']==1 && $_POST["Submit" ]=="Submit") {
    $checkbox1 = $_POST['chkl'] ;
    $checkbox2 = implode(', ', $checkbox1);
    $trn_date = date("Y-m-d H:i:s");
    $name = $_REQUEST['name'];
    $age = $_REQUEST['age'];
    $interval = getAge($age);
    $submittedby = $_SESSION["username"];
    {
        $ins_query = "insert into new_record (`trn_date`,`name`,`DOB`,`age`,`submittedby`,`language`,`gender`,`Your_Interest`) values ('$trn_date','$name','$age','$interval','$submittedby','$checkbox2','$gender','$opp')";

        if (mysqli_query($con, $ins_query)) {
            echo "Record inserted successfully";
        } else {
            echo "Error inserted record: " . mysqli_error($con);
        }

        $status = "New Record Inserted Successfully.</br></br><a href='view.php'>View Inserted Record</a>";
    }
}
?>
<?php
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 10)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage
    echo("SESSION OUT!");
}
$_SESSION['LAST_ACTIVITY'] = time();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert New Record</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>
<div class="form">
<p><a href="dashboard.php">Dashboard</a> | <a href="view.php">View Records</a> | <a href="logout.php">Logout</a></p>

<div>
<h1>Insert New Record</h1>
<form name="form" method="post" action=""> 
<input type="hidden" name="new" value="1" />
<p><input type="text" name="name" placeholder="Enter Name" required /></p>
    <label>Enter Language: </label><br>
    <input type="checkbox" name="chkl[]" value="php">php<br />
    <input type="checkbox" name="chkl[]" value="java">java<br />
    <input type="checkbox" name="chkl[]" value="c++">c++<br />
    <br>
    <label>Gender: </label><br>
    <input type="radio" name="sex" value="Male"/> Male
    <input type="radio" name="sex" value="Female"/>Female
    <br><br>
    <select name="op" required="required" >
        <option value="">What is your major field of interest?</option>
        <option value="coding">coding</option>
        <option value="graphic">graphic designing</option>
        <option value="managing">managing</option>
        <option value="Extra Curricular Activities">Extra Curricular Activities</option>
    </select>

    <br><br>
    <label for="start"><b>Enter Age:</b></label>

    <input type="date" id="start" name="age"
           value="2018-07-22"
           min="1880-01-01" max="2012-12-31">
    <br><br>
    <b>Select image to upload:</b>
    <br><br>
    <input type="file" name="fileToUpload" id="fileToUpload">
    <p><input name="Submit" type="submit" value="Submit" /></p>



</form>
<p style="color:#FF0000;"><?php echo $status; ?></p>

<br /><br /><br /><br />

</div>
</div>
</body>
</html>
